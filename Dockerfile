# do not use https://hub.docker.com/_/python as there is error when building uwsgi by pip
FROM alpine

MAINTAINER Marek Rychly <marek.rychly@gmail.com>

# https://github.com/cschwede/django-swiftbrowser/releases
ARG APP_VERSION=e1dcce490ed5fa2aa29fd51f0c065d562421c4fd

ARG DOWNLOAD_CACHE
ARG APP_URI="https://github.com/cschwede/django-swiftbrowser/archive/%s.tar.gz"

ENV \
APP_HOME="/opt/django-swiftbrowser"

RUN true \
# py3-django uwsgi-python3 py3-tox: from app requirements
# py3-pip: to install missing requirements
&& apk add --no-cache --update py3-django uwsgi-python3 uwsgi-http py3-tox py3-pip \
&& pip3 install python-swiftclient \
\
# clean up
&& rm -rf /tmp/* /var/tmp/* /var/cache/apk/*

COPY scripts /

RUN true \
# make the scripts executable
&& chmod 755 /*.sh \
# download the package
&& APP_URI=$(printf "${APP_URI}" "${APP_VERSION}") \
&& APP_FILE=${APP_URI##*/} \
&& ( [ -n "${DOWNLOAD_CACHE}" ] && cp -v "${DOWNLOAD_CACHE}/${APP_FILE}" /tmp \
	|| wget -O /tmp/${APP_FILE} "${APP_URI}" ) \
\
# extract the package and remove garbage
&& mkdir -p "${APP_HOME}" \
&& tar -xzf "/tmp/${APP_FILE}" -C "${APP_HOME}" --strip-components 1 \
\
# build
&& cd "${APP_HOME}" \
&& sed -i -e 's/py35/py3/' -e 's/python3\.5/python3/' tox.ini \
&& python3 setup.py install \
&& chown -R nobody "${APP_HOME}" \
\
# clean up
&& rm -rf /tmp/* /var/tmp/* /var/cache/apk/*

ENTRYPOINT ["/entrypoint.sh"]

HEALTHCHECK CMD /healthcheck.sh
