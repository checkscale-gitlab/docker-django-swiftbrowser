#!/bin/sh

export ALLOWED_HOSTS="*"
export BASE_URL="http://127.0.0.1:${APP_PORT:-8000}"

if [ $# -gt 0 ]; then
	# custom
	exec $@
else
	# default
	exec uwsgi \
		--plugins http,python3 \
		--http ":${APP_PORT:-8000}" \
		--uid nobody --gid nobody \
		--chdir "${APP_HOME:-/opt/django-swiftbrowser}" \
		--module swiftbrowser.wsgi:application \
		--master --processes "${APP_PROCESSES:-2}" --threads "${APP_THREADS:-2}" \
		--socket /tmp/uswgi.sock
fi
